'use strict';

module.exports = function (str, margin) {
	if (!margin) {
		margin = 0;
	}

	margin = ' '.repeat(margin);
	const padding = 4;
	const len = str.length + padding * 2;
	const border = '+' + '-'.repeat(len) + '+';

	return margin + border + '\n'
		+ margin + '|' + ' '.repeat(padding) + str + ' '.repeat(padding) + '|' + '\n'
		+ margin + border;
};
