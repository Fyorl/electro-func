#!/usr/bin/env node

const spawn = require('child_process').spawn;
const runner = `${__dirname}/runner.js`;
let electron = `${__dirname}/../.bin/electron`;

if (process.platform === 'win32') {
	electron += '.cmd';
}

spawn(electron, [runner, '--color'].concat(process.argv.slice(2)), {stdio: 'inherit'});
