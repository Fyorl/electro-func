'use strict';

const titleField = $('#edit-title-field');
const titleDisplay = $('#display-title');
const addItemBtn = $('#add-item');

let itemIndex = 0;

const isChecked = id => {
	return localStorage['item-checked-' + id] === 'true';
};

const populateItems = () => {
	const ids =
		Object.keys(localStorage)
		.filter(key => key.substr(0, 10) === 'item-text-')
		.map(key => parseInt(key.split('-')[2]));
	ids.sort((a, b) => a - b);

	ids.forEach(id => {
		itemIndex = id;
		addItem(id, localStorage['item-text-' + id], isChecked(id));
	});

	itemIndex++;
};

const start = () => {
	titleField.val(localStorage['title'] || 'Untitled');
	titleDisplay.text(localStorage['title'] || 'Untitled');
	populateItems();
};

const editTitle = () => {
	const titleField = $('#edit-title-field');
	titleField.show();
	titleField[0].focus();
	titleField[0].setSelectionRange(0, titleField.val().length);
	titleDisplay.hide();
};

const saveTitle = e => {
	if (e.keyCode === 13) {
		const newTitle = titleField.val() || 'Untitled';
		titleField.hide();
		titleDisplay.show();
		titleDisplay.text(newTitle);
		localStorage['title'] = newTitle;
	}
};

const clone = id => {
	const el = $('#clone-' + id);

	if (el.length < 1) {
		throw new Error('Clone "' + id + '" does not exist.');
	}

	return el.clone().children();
};

const itemID = s => {
	return s.split('-')[1];
};

const itemIDFromEvent = e => {
	return itemID($(e.target).parent().attr('id'));
};

const removeItem = id => {
	const itemDisplay = $('#item-' + id);
	itemDisplay.remove();
	localStorage.removeItem('item-text-' + id);
	localStorage.removeItem('item-checked-' + id);
};

const saveItemText = e => {
	if (e.keyCode === 13) {
		const id = itemIDFromEvent(e);
		const itemTextField = $(e.target);
		const itemTextDisplay = itemTextField.siblings('span');
		const newText = itemTextField.val();

		if (newText.length < 1) {
			removeItem(id);
		} else {
			itemTextField.hide();
			itemTextDisplay.show();
			itemTextDisplay.text(newText);
			localStorage['item-text-' + id] = newText;
		}
	}
};

const editItemText = e => {
	const itemTextDisplay = $(e.target);
	const itemTextField = itemTextDisplay.siblings('input');
	itemTextDisplay.hide();
	itemTextField.show();
	itemTextField[0].focus();
	itemTextField[0].setSelectionRange(0, itemTextField.val().length);
};

const toggleState = e => {
	const id = itemIDFromEvent(e);
	const itemCheckbox = $(e.target);
	const checked = !isChecked(id);
	itemCheckbox.removeClass('fa-check-square-o').removeClass('fa-square-o');
	itemCheckbox.addClass(checked ? 'fa-check-square-o' : 'fa-square-o');
	localStorage['item-checked-' + id] = checked ? 'true' : 'false';
};

const addItem = (id, text, checked) => {
	const newItem = clone('todo-item').attr('id', 'item-' + (id || itemIndex));
	const itemTextField = newItem.find('input');
	const itemTextDisplay = newItem.find('span');
	const itemCheckbox = newItem.find('i');
	addItemBtn.parent().before(newItem);
	itemTextField.keypress(saveItemText);
	itemTextDisplay.click(editItemText);
	itemTextField.val(text || 'New item');
	itemTextDisplay.text(text || 'New item');
	itemCheckbox.addClass(checked ? 'fa-check-square-o' : 'fa-square-o');
	itemCheckbox.click(toggleState);
	itemTextField[0].focus();
	itemTextField[0].setSelectionRange(0, itemTextField.val().length);

	itemTextDisplay.hide();
	itemTextField.hide();

	if (id == null) {
		itemTextField.show();
		itemIndex++;
	} else {
		itemTextDisplay.show();
	}
};

const addNewItem = () => {
	localStorage['item-text-' + itemIndex] = 'New item';
	localStorage['item-checked-' + itemIndex] = 'false';
	addItem();
};

titleDisplay.click(editTitle);
titleField.keypress(saveTitle);
addItemBtn.click(addNewItem);
start();
