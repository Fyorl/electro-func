'use strict';

_tests.Initialisation = {
	_before: done => {
		clearLocalStorage();
		resetPage();
		done();
	}
};

_tests.Initialisation.testNoData = done => {
	start();

	expect($('#edit-title-field').val()).to.equal('Untitled');
	expect($('#display-title').text()).to.equal('Untitled');
	expect($('#list').find('li')).to.have.length(1);
	expect(itemIndex).to.equal(1);

	done();
};

_tests.Initialisation.testHasData = done => {
	localStorage['title'] = 'test';
	localStorage['item-checked-0'] = 'false';
	localStorage['item-text-0'] = 'item-0';
	localStorage['item-checked-42'] = 'true';
	localStorage['item-text-42'] = 'item-42';

	start();

	const list = $('#list').find('li');
	expect($('#edit-title-field').val()).to.equal('test');
	expect($('#display-title').text()).to.equal('test');
	expect(list).to.have.length(3);
	expect($(list[0]).find('i').hasClass('fa-square-o')).to.be.true;
	expect($(list[0]).find('span').text()).to.equal('item-0');
	expect($(list[0]).find('input').val()).to.equal('item-0');
	expect($(list[1]).find('i').hasClass('fa-check-square-o')).to.be.true;
	expect($(list[1]).find('span').text()).to.equal('item-42');
	expect($(list[1]).find('input').val()).to.equal('item-42');
	expect(itemIndex).to.equal(43);

	done();
};
