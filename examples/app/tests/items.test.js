'use strict';

_tests.Items = {
	_before: done => {
		clearLocalStorage();
		resetPage();
		done();
	}
};

_tests.Items.testAddItem = done => {
	addItem(42, 'abc', false);
	addItem(43, 'def', true);
	addItem();

	const list = $('#list');
	const items = list.find('li');

	expect(items).to.have.length(4);
	expect($(items[0]).attr('id')).to.equal('item-42');
	expect($(items[0]).find('input').val()).to.equal('abc');
	expect($(items[0]).find('input').css('display')).to.equal('none');
	expect($(items[0]).find('i').hasClass('fa-square-o')).to.be.true;
	expect($(items[1]).attr('id')).to.equal('item-43');
	expect($(items[1]).find('input').val()).to.equal('def');
	expect($(items[1]).find('input').css('display')).to.equal('none');
	expect($(items[1]).find('i').hasClass('fa-check-square-o')).to.be.true;
	expect($(items[2]).attr('id')).to.equal('item-0');
	expect($(items[2]).find('input').val()).to.equal('New item');
	expect($(items[2]).find('input').css('display')).to.equal('inline-block');
	expect($(items[2]).find('i').hasClass('fa-square-o')).to.be.true;
	expect(itemIndex).to.equal(1);

	done();
};

_tests.Items.testAddNewItem = done => {
	addNewItem();
	addNewItem();
	addNewItem();

	[0, 1, 2].forEach(n => {
		expect(localStorage['item-text-' + n]).to.exist;
		expect(localStorage['item-checked-' + n]).to.exist;
	});

	expect(itemIndex).to.equal(3);

	done();
};

_tests.Items.testRemoveItem = done => {
	addNewItem();
	addNewItem();
	removeItem(0);

	expect($('#item-0')).to.have.length(0);
	expect(localStorage['item-text-0']).to.not.exist;
	expect(localStorage['item-checked-0']).to.not.exist;
	expect($('#item-1')).to.have.length(1);
	expect(localStorage['item-text-1']).to.exist;
	expect(localStorage['item-checked-1']).to.exist;
	expect(itemIndex).to.equal(2);

	done();
};

_tests.Items.testEditItemText = done => {
	addItem(0, 'abc', false);

	const item = $('#item-0');
	const span = item.find('span');
	const input = item.find('input');
	const click = $.Event('click');
	click.target = span[0];
	span.trigger(click);

	expect(span.css('display')).to.equal('none');
	expect(input.css('display')).to.equal('inline-block');

	done();
};

_tests.Items.testSaveItemText = done => {
	addItem(0, 'abc', false);

	const item = $('#item-0');
	const span = item.find('span');
	const input = item.find('input');

	const click = $.Event('click');
	click.target = span[0];

	const pressEnter = $.Event('keypress');
	pressEnter.keyCode = 13;

	span.trigger(click);
	input.val('def');
	input.trigger(pressEnter);

	expect(span.css('display')).to.equal('inline');
	expect(span.text()).to.equal('def');
	expect(input.css('display')).to.equal('none');
	expect(localStorage['item-text-0']).to.equal('def');

	span.trigger(click);
	input.val('');
	input.trigger(pressEnter);

	expect($('#item-0')).to.have.length(0);
	expect(localStorage['item-text-0']).to.not.exist;
	expect(localStorage['item-checked-0']).to.not.exist;

	done();
};

_tests.Items.testToggleState = done => {
	addItem(0, 'abc', false);

	const item = $('#item-0');
	const checkbox = item.find('i');
	const click = $.Event('click');
	click.target = checkbox[0];

	checkbox.trigger(click);
	expect(isChecked(0)).to.be.true;
	expect(checkbox.hasClass('fa-square-o')).to.be.false;
	expect(checkbox.hasClass('fa-check-square-o')).to.be.true;

	checkbox.trigger(click);
	expect(isChecked(0)).to.be.false;
	expect(checkbox.hasClass('fa-square-o')).to.be.true;
	expect(checkbox.hasClass('fa-check-square-o')).to.be.false;

	done();
};
