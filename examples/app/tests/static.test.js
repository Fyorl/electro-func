'use strict';

_tests.Static = {};

_tests.Static.testIsChecked = done => {
	clearLocalStorage();

	localStorage['item-checked-9999'] = 'true';
	localStorage['item-checked-9998'] = 'false';
	localStorage['item-checked-9997'] = '1';

	expect(isChecked(9999)).to.be.true;
	expect(isChecked(9998)).to.be.false;
	expect(isChecked(9997)).to.be.false;
	expect(isChecked(404)).to.be.false;

	done();
};

_tests.Static.testClone = done => {
	const li = clone('todo-item');

	expect(li).to.have.length(1);
	expect(li[0].tagName).to.equal('LI');
	expect(li.children('i')).to.have.length(1);
	expect(li.children('span')).to.have.length(1);
	expect(li.children('input')).to.have.length(1);

	done();
};

_tests.Static.testBadClone = done => {
	let exceptionThrown = false;

	try {
		clone('404');
	} catch (e) {
		expect(e.message).to.equal('Clone "404" does not exist.');
		exceptionThrown = true;
	}

	expect(exceptionThrown).to.be.true;

	done();
};

_tests.Static.testItemID = done => {
	expect(itemID('#item-0')).to.equal('0');
	expect(itemID('#item-9999')).to.equal('9999');
	done();
};

_tests.Static.testItemIDFromEvent = done => {
	const item1 = clone('todo-item');
	const item2 = clone('todo-item');

	item1.attr('id', 'item-1');
	item2.attr('id', 'item-2');

	const e1 = {target: item1.find('span')[0]};
	const e2 = {target: item2.find('span')[0]};

	expect(itemIDFromEvent(e1)).to.equal('1');
	expect(itemIDFromEvent(e2)).to.equal('2');

	done();
};
