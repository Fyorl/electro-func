'use strict';

const click = $.Event('click');
const pressEnter = $.Event('keypress');
pressEnter.keyCode = 13;

_tests.Title = {
	_before: done => {
		clearLocalStorage();
		resetPage();
		done();
	}
};

_tests.Title.testEditTitle = done => {
	titleDisplay.trigger(click);
	expect(titleDisplay.css('display')).to.equal('none');
	expect(titleField.css('display')).to.equal('inline-block');

	titleField.trigger(pressEnter);
	done();
};

_tests.Title.testSaveBlankTitle = done => {
	titleDisplay.trigger(click);
	titleField.val('');
	titleField.trigger(pressEnter);

	expect(localStorage['title']).to.equal('Untitled');
	expect(titleDisplay.text()).to.equal('Untitled');
	expect(titleDisplay.css('display')).to.equal('inline-block');
	expect(titleField.css('display')).to.equal('none');

	done();
};

_tests.Title.testSaveTitle = done => {
	titleDisplay.trigger(click);
	titleField.val('test');
	titleField.trigger(pressEnter);

	expect(localStorage['title']).to.equal('test');
	expect(titleDisplay.text()).to.equal('test');
	expect(titleDisplay.css('display')).to.equal('inline-block');
	expect(titleField.css('display')).to.equal('none');

	done();
};
