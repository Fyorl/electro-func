'use strict';

const clearLocalStorage = () => {
	Object.keys(localStorage).forEach(key => {
		localStorage.removeItem(key);
	});
};

const resetPage = () => {
	itemIndex = 0;
	const addBtnLI = $('#add-item').parent();
	$('#list').find('li').filter((i, el) => {
		return el !== addBtnLI[0];
	}).remove();
};
