'use strict';

const loadFavouriteColour = () => {
	const colour = localStorage['favourite-colour'];
	const msg =
		colour
			? 'Your favourite colour is ' + colour
			: "You don't have a favourite colour yet";
	$('#greeting').text(msg);
};

const saveFavouriteColour = () => {
	const colour = $('#colour').val();
	if (colour.length < 1) {
		localStorage.removeItem('favourite-colour');
	} else {
		localStorage['favourite-colour'] = colour;
	}
};

$('#save').click(saveFavouriteColour);
loadFavouriteColour();
