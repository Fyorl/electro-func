'use strict';

// Create the test suite.
_tests.Site = {};

// Clean up any saved state before each test.
_tests.Site._before = done => {
	Object.keys(localStorage).forEach(key => localStorage.removeItem(key));
	done();
};

_tests.Site.testLoadNoFavouriteColour = done => {
	loadFavouriteColour();
	assert.equal($('#greeting').text(), "You don't have a favourite colour yet");
	done();
};

_tests.Site.testLoadFavouriteColour = done => {
	localStorage['favourite-colour'] = 'black';
	loadFavouriteColour();
	assert.equal($('#greeting').text(), 'Your favourite colour is black');
	done();
};

_tests.Site.testSaveFavouriteColour = done => {
	const click = $.Event('click');
	$('#colour').val('black');
	$('#save').trigger(click);
	assert.equal(localStorage['favourite-colour'], 'black');
	done();
};

_tests.Site.testSaveBlankFavouriteColour = done => {
	const click = $.Event('click');
	$('#colour').val('');
	$('#save').trigger(click);
	expect(localStorage['favourite-colour']).to.not.exist;
	done();
};
