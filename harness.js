'use strict';

let remote;
let assert;
let expect;
let moment;
let timeoutms;

let lastTest;
let lastSuites;
let lastSuite;
let lastTests;

let _tests = {};

if (window.require) {
	remote = require('electron').remote;
	assert = require('chai').assert;
	expect = require('chai').expect;
	moment = require('moment');
	timeoutms = remote.getGlobal('_timeout');

	window.addEventListener('error', error => remote.getGlobal('_error')(error));
	window.addEventListener('unhandledrejection', e => {
		remote.getGlobal('_testFailed')(lastTest, e.reason.message || 'Unknown error', 0);
		_tests._doNextTest();
	});
}

const _harness = function () {
	this._timeout = null;

	this._startTimeout = () => {
		return setTimeout(() => {
			remote.getGlobal('_timedOut')(lastTest);
		}, timeoutms);
	};

	this._run = () => {
		const suites = Object.keys(this).filter(suite => !suite.startsWith('_'));
		this._nextSuite(suites);
	};

	this._nextSuite = suites => {
		if (suites.length < 1) {
			clearTimeout(this._timeout);
			remote.getGlobal('_testingComplete')();
			return;
		}

		const suite = suites.shift();
		remote.getGlobal('_newSuite')(suite);
		const tests = Object.keys(this[suite]).filter(test => !test.startsWith('_'));

		if (this[suite]._beforeSuite) {
			try {
				this[suite]._beforeSuite(() => this._nextTest(suites, this[suite], tests));
			} catch (e) {
				remote.getGlobal('_setupFailed')('_beforeSuite', e.message || 'Unknown error');
			}
		} else {
			this._nextTest(suites, this[suite], tests);
		}
	};

	this._nextTest = (suites, suite, tests) => {
		if (this._timeout !== null) {
			clearTimeout(this._timeout);
		}
		this._timeout = this._startTimeout();

		if (tests.length < 1) {
			if (suite._afterSuite) {
				try {
					suite._afterSuite(() => this._nextSuite(suites));
				} catch (e) {
					remote.getGlobal('_setupFailed')('_afterSuite', e.message || 'Unknown error');
				}
			} else {
				this._nextSuite(suites);
			}

			return;
		}

		const test = tests.shift();
		const startTime = moment.utc();

		lastSuites = suites;
		lastSuite = suite;
		lastTests = tests;
		lastTest = test;

		const testPassed = () => {
			const endTime = moment.utc();
			remote.getGlobal('_testPassed')(test, endTime.diff(startTime));
			this._doNextTest();
		};

		const testFailed = e => {
			const endTime = moment.utc();
			remote.getGlobal('_testFailed')
			(test, e.message || 'Unknown error', endTime.diff(startTime));
			this._doNextTest();
		};

		const runTest = () => {
			try {
				suite[test](testPassed);
			} catch (e) {
				testFailed(e);
			}
		};

		if (suite._before) {
			try {
				suite._before(runTest);
			} catch (e) {
				remote.getGlobal('_setupFailed')('_before', e.message || 'Unknown error');
			}
		} else {
			runTest();
		}
	};

	this._doNextTest = () => {
		if (lastSuite._after) {
			try {
				lastSuite._after(() => this._nextTest(lastSuites, lastSuite, lastTests));
			} catch (e) {
				remote.getGlobal('_setupFailed')('_after', e.message || 'Unknown error');
			}
		} else {
			this._nextTest(lastSuites, lastSuite, lastTests);
		}
	};
};

if (window.require) {
	_tests = new _harness();
}
