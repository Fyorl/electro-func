'use strict';

const electron = require('electron');
const colours = require('colors');
const basename = require('path').basename;
const fs = require('fs');
const art = require('./art');
const args = require('minimist')(process.argv.slice(2), {boolean: ['color', 'debug', 'help']});

let win;
let totalTests = 0;
let testsPassed = 0;
let testsFailed = 0;
let totalMS = 0;

if (args['help']) {
	console.log('Usage: electro-func [OPTIONS] [FILE]');
	console.log();
	console.log('File:');
	console.log('  The name of the file (without the extension) that contains');
	console.log('  the tests you want to run. If no FILE is specified,');
	console.log('  electro-func will look for TestRunner.html.');
	console.log();
	console.log('Options:');
	console.log('  --debug    Spawn a renderer window of your test and use the');
	console.log('             dev tools to debug it.');
	console.log('  --timeout  Specify the number of milliseconds to wait before');
	console.log('             terminating a test. Default 2000.');
	electron.app.quit();
}

const quit = () => {
	if (args['debug']) {
		totalTests = 0;
		testsPassed = 0;
		testsFailed = 0;
		totalMS = 0;
	} else {
		electron.app.quit();
	}
};

global._timeout = 2000;

if (args['timeout'] && typeof(args['timeout']) === 'number') {
	global._timeout = args['timeout'];
}

global._error = function (error) {
	console.log();
	console.log('  ' + (basename(error.filename) + ':' + error.lineno + ': ' + error.message).red);
};

global._timedOut = function (test) {
	console.log();
	console.log(('  `' + test + '` took too long to execute (>' + global._timeout + 'ms).').red);
	quit();
};

global._testingComplete = function () {
	if (totalTests > 0) {
		console.log();
		let summary = '';
		if (testsPassed === totalTests) {
			summary += ('  ' + totalTests + ' passing').green;
		} else {
			summary += ('  ' + testsFailed + ' failing').red;
			summary += '  ' + (testsPassed + ' passing').green;
		}
		summary += ('  (' + totalMS + 'ms)').grey;
		console.log(summary);
	}

	quit();
};

global._newSuite = function (suite) {
	console.log();
	console.log('  ' + suite);
};

global._testPassed = function (test, ms) {
	totalTests++;
	testsPassed++;
	totalMS += ms;
	console.log('    ✔ '.green + (test + ' (' + ms + 'ms)').grey);
};

global._testFailed = function (test, errmsg, ms) {
	totalTests++;
	testsFailed++;
	totalMS += ms;
	console.log('    ✘ '.red + test.grey);
	console.log('        ' + errmsg.red.bold)
};

global._setupFailed = function (step, errmsg) {
	console.log('    ' + (step + ' failed: ' + errmsg).red);
};

electron.app.commandLine.appendSwitch('ignore-certificate-errors');
electron.app.on('ready', () => {
	const filename = (args._.length > 0 ? args._[0] : 'TestRunner') + '.html';
	const htmlRunner = process.cwd() + '/' + filename;

	if (!fs.existsSync(htmlRunner)) {
		console.error('HTML test runner `' + htmlRunner + '` does not exist.');
		electron.app.quit();
		return;
	}

	console.log(art(filename.toUpperCase(), 2));
	win = new electron.BrowserWindow({
		width: 1024
		, height: 768
		, show: args['debug']
		, webPreferences: {
			webSecurity: false
		}
	});
	win.loadURL('file://' + htmlRunner);

	if (args['debug']) {
		win.webContents.openDevTools({mode: 'bottom'});
	}

	win.webContents.on('dom-ready', () => win.webContents.executeJavaScript('_tests._run();'));
});
